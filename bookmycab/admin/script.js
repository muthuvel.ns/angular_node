app = angular.module('loginApp', []).config(function ($routeProvider) {
  $routeProvider
    .when('/dashboard', {
        templateUrl: 'home.html',
        controller: 'MyController',
        loginRequired: true 
      })
    .when('/login', {
        templateUrl: 'login.html',
        controller: 'MyControllerlogin'
      })
    .otherwise({redirectTo: '/login'})
});


