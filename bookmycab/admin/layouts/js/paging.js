var todos = angular.module('myApp', ['ui.bootstrap']);

todos.controller('MyController', function($scope) {
   $scope.items = []
  ,$scope.currentPage = 1
  ,$scope.numPerPage = 10
  ,$scope.maxSize = 5;
  
  $scope.makeTodos = function() {
    $scope.todos = [];
    for (i=1;i<=5;i++) {
      $scope.todos.push({ name:'item '+i, done:false});
    }
  };
  $scope.makeTodos(); 
  
  $scope.numPages = function () {
    return Math.ceil($scope.todos.length / $scope.numPerPage);
  };
  
  $scope.$watch('currentPage + numPerPage', function() {
    var begin = (($scope.currentPage - 1) * $scope.numPerPage)
    , end = begin + $scope.numPerPage;
    
    $scope.items = $scope.todos.slice(begin, end);
  });
});
