var app = angular.module('myApp', []);
        app.config(['$httpProvider', function($httpProvider) {
                $httpProvider.defaults.useXDomain = true;
                $httpProvider.defaults.headers.common = 'Content-Type: application/json';
                delete $httpProvider.defaults.headers.common['X-Requested-With'];
                }
        ]);
    app.controller('MyController', function($scope,$http) {


// Create payment
	   $scope.save = function(km,amount) {
	$scope.original = {km:km, amount:amount}; 
    $http.post('http://localhost:8085/calltaxi/v1/payment/createApi', $scope.original)
	.then(function(data) {
$scope.items=data;
    });   
  };   
	  
    // update payment Row
        $scope.updateRow = function(id) {
                $http.get("http://localhost:8085/calltaxi/v1/payment/singleview/?id="+id)
                .success(function(response){
                //console.log(response);
                $scope.del = response;
                })
               };
   // Delete payment
        $scope.deleteRow = function(id) {
                $http.get("http://localhost:8085/calltaxi/v1/payment/deleteApi/?id="+id)
                .success(function(response){
                //console.log(response);
                $scope.del = response;
                })
               };
// Single View payment
        $scope.singleRecord = function(id) {
                $http.get("http://localhost:8085/calltaxi/v1/payment/singleview/?id="+id)
                .success(function(response){
                $scope.items = response;
                })
               };
            //});
// Full View payment
    $http({
  method: 'POST',
  url: 'http://localhost:8085/calltaxi/v1/payment/fullviewApi'
}).then(function successCallback(response) {
//alert(response.records);
                var recordsArray = response.data.records;
            console.log(JSON.stringify(recordsArray))
            $scope.items = recordsArray;
  }, function errorCallback(response) {

  });





    });   // app.controller
