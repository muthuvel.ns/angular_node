var app=angular.module('myApp',['ngRoute']);


app.config(function($routeProvider){

      $routeProvider
          .when('/login',{
                templateUrl: 'index.html'
          })
          .when('/driver',{
                templateUrl: 'driver.html'
          })
		  .when('/contact',{
			  templateUrl: 'contact.php'
		  })
		  .when('/share',{
			  templateUrl: 'share.php'
		  });

});


app.controller('MyController',function($scope){

      $scope.message="Hello world";

});


