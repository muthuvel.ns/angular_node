 
var app = angular.module('myApp', []);
        app.config(['$httpProvider', function($httpProvider) {
                $httpProvider.defaults.useXDomain = true;
                $httpProvider.defaults.headers.common = 'Content-Type: application/json';
                delete $httpProvider.defaults.headers.common['X-Requested-With'];
                }
        ]);
    app.controller('MyController', function($scope,$http) {
   // Create car
	$scope.save = function(cid,uname,model,color,number) {
	$scope.original = {cid:cid,name:uname, model:model, color:color, number:number}; 
    $http.post('http://localhost:8085/calltaxi/v1/car/createApi', $scope.original)
	.then(function(response) {
                console.log(response.data);
                if(response.data.result =='Success'){
                    $scope.del = response.data;      
                $location.path("cars");
                 }
    });   
  };   
	  
    // update car Row
        $scope.updateRow = function(id) {
                $http.get("http://localhost:8085/calltaxi/v1/car/singleview/?id="+id)
                .success(function(response){
                //console.log(response.data);
                if(response.data.result =='Success'){
                    $scope.del = response.data;      
                $location.path("cars");
                 }
                })
               };
   // Delete car
        $scope.deleteRow = function(id) {
                $http.get("http://localhost:8085/calltaxi/v1/car/deleteApi/?id="+id)
                .success(function(response){
                if(response.result =='Success'){
                    $scope.del = response;      
                $location.path("booking");
                 }
                })
               };
// Single View car
        $scope.singleRecord = function(id) {
                $http.get("http://localhost:8085/calltaxi/v1/car/singleview/?id="+id)
                .success(function(response){
                $scope.items = response;
                })
               };
            //});
// Full View car
    $http({
  method: 'GET',
  url: 'http://localhost:8085/calltaxi/v1/car/fullviewApi'
}).then(function successCallback(response) {
//alert(response.records);
                var recordsArray = response.data.records;
            console.log(JSON.stringify(recordsArray))
            $scope.items = recordsArray;
  }, function errorCallback(response) {

  });





    });   // app.controller
