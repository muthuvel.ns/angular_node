/*
URL : http://localhost:7071/calltaxi/v1/driver/
1 Create New Driver Api --- createApi
2 Driver Full View Api --- fullvieweApi
3 Driver update Api --- updateApi
4 Driver Records Delete Api --- deleteApi
5 Driver Records Single View Api --- singleview
*/

var app = require('express')(); // Express App include
var http = require('http').Server(app); // http server
var mysql = require('mysql'); // Mysql include
//var mailer = require("nodemailer");
var bodyParser = require("body-parser"); // Body parser for fetch posted data
var connection = mysql.createConnection({ // Mysql Connection
    host : 'localhost',
    user : 'root',
    password : 'root',
    database : 'cabdb',
});
var request = require('request');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// 4 Driver Records Delete Api
app.get('/calltaxi/v1/driver/deleteApi', function(req,res){  
res.header("Access-Control-Allow-Origin", "*");
res.header("Access-Control-Allow-Headers", "Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With");
res.header("Access-Control-Allow-Methods", "GET, PUT, POST");
    var id = req.query.id;
    var data = { "Status":"", };
console.log(req.query);
if(id != '') {

 var query = connection.query('DELETE from driver WHERE id=?', id, function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Driver Cannot Delete";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("Delete Query  "+err);
            }else{                  		   
                data["Status"] = "Driver Deleted Successfully";
                data["result"] = "Success";             
				res.json(data);
        }  // Update query        
    });   // Insert query
 }else{
        data["Status"] = "Please enter all required data";
        data["result"] = "failure";
        res.json(data);
   } 
});





app.listen(8085);
console.log('port 8085');

