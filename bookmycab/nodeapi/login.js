/*
URL : http://localhost:8085/calltaxi/v1/driver/
1 Create New Driver Api --- createApi
2 Driver Full View Api --- fullvieweApi
3 Driver update Api --- updateApi
4 Driver Records Delete Api --- deleteApi
5 Driver Records Single View Api --- singleview
*/

var app = require('express')(); // Express App include
var http = require('http').Server(app); // http server
var mysql = require('mysql'); // Mysql include
//var mailer = require("nodemailer");
var bodyParser = require("body-parser"); // Body parser for fetch posted data
var connection = mysql.createConnection({ // Mysql Connection
    host : 'localhost',
    user : 'root',
    password : 'root',
    database : 'cabdb',
});
var crypto = require('crypto');
var request = require('request');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(function(req, res, next) {
res.setHeader('Access-Control-Allow-Origin', '*');
res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
next();
}); 

//  Admin Login Api
app.post('/calltaxi/v1/admin/login', function(req,res,next){  

    var uname = req.body.uname;
    var pwd = req.body.pwd;

    var data = { "Status":"", };
console.log(req.body);
if((uname != '') && (pwd != '')) {
 
var cryptpwd= crypto.createHash('md5').update(pwd).digest("hex");

 var query = connection.query('SELECT * from loginadmin WHERE username=? and password=?', [uname,cryptpwd], function(err, rows, fields){
            if(err){                               			   
               data["Status"] = "Admin Login is Error";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("View Query  "+err);
				res.json(data);
              }
            else if(rows.length != 0){
                 data["Status"] = "Admin Login successfully";
                data["result"] = "Success";     
                data["id"] = rows[0].id; 
                data["uname"] = rows[0].username;
				res.json(data);  
            }else{   
                data["Status"] = "Please enter correct username and password";
                data["result"] = "Success";
                console.log(query.sql);
				res.json(data);       

        }  // else  

    });   // Insert query
				//res.json({"data":data}); 
 }else{
        data["Status"] = "Please enter all required data";
        data["result"] = "success";
        res.json(data);
   } 
});


app.listen(8089);
console.log('port 8089');

