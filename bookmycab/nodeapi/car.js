/* 
// Booking
URL : http://localhost:8085/calltaxi/v1/booking/
1 Booking Delete Api  --- deleteApi
2 Booking Full View Api --- fullvieweApi
3 Booking Records Single View Api --- singleview
// Driver
URL : http://localhost:8085/calltaxi/v1/driver/
1 Create New Driver Api --- createApi
2 Driver Full View Api --- fullvieweApi
3 Driver update Api --- updateApi
4 Driver Records Delete Api --- deleteApi
5 Driver Records Single View Api --- singleview
*/

var app = require('express')(); // Express App include
var http = require('http').Server(app); // http server
var mysql = require('mysql'); // Mysql include
//var mailer = require("nodemailer");
var bodyParser = require("body-parser"); // Body parser for fetch posted data
var connection = mysql.createConnection({ // Mysql Connection
    host : 'localhost',
    user : 'root',
    password : 'root',
    database : 'cabdb',
});
var crypto = require('crypto');
var request = require('request');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(function(req, res, next) {
res.setHeader('Access-Control-Allow-Origin', '*');
res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
next();
});


// 1 Car Full View Api
app.get('/calltaxi/v1/booking/fullvieweApi', function(req,res){   
    var data = { "Status":"", };
//console.log(req.body);
 var query = connection.query('SELECT * from book', function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Cannot Search booking View";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("SELECT Query  "+err);
				res.json({"data":data});
            }else{                   
				
				data['records']=rows;
                data["Status"] = "Booking select Successfully";
                data["result"] = "Success";             
				res.json(data);			    
            // res.json(data);
        }  // Update query
        
    });   // Insert query
});


// 2 Car Records Delete Api
app.get('/calltaxi/v1/booking/deleteApi', function(req,res){  
    var id = req.query.id;
    var data = { "Status":"", };
console.log(req.query);
if((id != '') && (id >= 1)) {
 var query = connection.query('DELETE from book WHERE id=?', id, function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Booking Cannot Delete";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("Delete Query  "+err);
				res.json({"data":data});
            }else{                  		   
                data["Status"] = "Booking Deleted Successfully";
                data["result"] = "Success";             
				res.json(data);
        }  // Update query        
    });   // Insert query
 }else{
        data["Status"] = "Please enter all required data";
        data["result"] = "failure";
				res.json({"data":data});
   } 
});

// 3 Booking Records Single View Api
app.get('/calltaxi/v1/booking/singleview', function(req,res){  
    var id = req.query.id;

    var data = { "Status":"", };
//console.log(req.query);
if(id != '') {
 var query = connection.query('SELECT * from book WHERE id=?', id, function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Booking View is Cannot Display";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("View Query  "+err);
				res.json({"data":data});
            }else{         
                //res.writeHead(302, {'Location': 'http://localhost/bookmycab/cabadmin/home.html' + req.url});
                //res.end();           			   
                data["Status"] = "Booking Seleted Successfully";
                data["result"] = "Success";     
                data["records"] = rows;                 
				res.json(data);    

        }  // Update query        
    });   // Insert query
 }else{
        data["Status"] = "Please enter all required data";
        data["result"] = "failure";
				res.json({"data":data});
   } 
});

// 1. Create New Driver Api
app.post('/calltaxi/v1/driver/createApi', function(req,res){    
    var name = req.body.uname;
    var mobile = req.body.mobile;
    var location = req.body.location;
    var email = req.body.email;  
var pos = {
        name:name,
        mobile: mobile,
        email: email,
        location: location
    };
    var data = { "Status":"", };
// console.log(req.body);
if((name != '') && (mobile != '')) {

 var query = connection.query('INSERT INTO driver SET ?', pos, function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Driver Cannot Create";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("Insert Query  "+err);
				res.json({"data":data});
            }else{  
                 connection.query('update driver set did=CONCAT("CD", LAST_INSERT_ID()) where id=LAST_INSERT_ID()', 
                 function(err, rows, fields){
                 if(!!err){
				data["result"] = "failure";
                console.log("Update Query Error  "+err);
				res.json({"data":data});
			    }else{					
                data["Status"] = "Driver Created Successfully";				
                data["result"] = "Success";             
				res.json(data);
			    } //res.json(data);
            }); // Update query            
        }         
    });   
 }else{
        data["Status"] = "Please enter all required data";
        data["result"] = "failure";
        res.json(data);
		} 
	});


// 2 Driver Full View Api
app.get('/calltaxi/v1/driver/fullvieweApi', function(req,res){   
    var data = { "Status":"", };
//console.log(req.body);
 var query = connection.query('SELECT * from driver', function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Cannot Search Driver View";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("SELECT Query  "+err);
				res.json({"data":data});
            }else{                   
				
				data['records']=rows;
                data["Status"] = "Driver select Successfully";
                data["result"] = "Success";             
				res.json(data);			    
            // res.json(data);
        }  // Update query
        
    });   // Insert query
});



// 3 Driver update Api updateApi
app.post('/calltaxi/v1/driver/updateApi', function(req,res){ 
    var id = req.body.id;
	var name = req.body.name;
    var mobile = req.body.mobile;
    var location = req.body.location;
    var email = req.body.email; 
var pos = {
        id:id,
        name:name,
        mobile: mobile,
        email: email,
        location: location
    };
    var data = { "Status":"", };
//console.log(req.body);
if(id != '')  {
 var query = connection.query('UPDATE driver SET ? WHERE id=?', [pos,id], function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Driver Cannot Update";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("UPDATE Query  "+err);
            }else{                   			   
                data["Status"] = "Driver Updated Successfully";
                data["result"] = "Success"; 
                //console.log(query.sql)            
				res.json(data);
        }  // Update query        
    });   // Insert query
 }else{
        data["Status"] = "Please enter all required data";
        data["result"] = "failure";
				res.json({"data":data});
		} 
	});




// 4 Driver Records Delete Api
app.get('/calltaxi/v1/driver/deleteApi', function(req,res){  
    var id = req.query.id;
    var data = { "Status":"", };
console.log(req.query);
if((id != '') && (id >= 1)) {
 var query = connection.query('DELETE from driver WHERE id=?', id, function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Driver Cannot Delete";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("Delete Query  "+err);
				res.json({"data":data});
            }else{                  		   
                data["Status"] = "Driver Deleted Successfully";
                data["result"] = "Success";             
				res.json(data);
        }  // Update query        
    });   // Insert query
 }else{
        data["Status"] = "Please enter all required data";
        data["result"] = "failure";
				res.json({"data":data});
   } 
});



// 5 Driver Records Single View Api
app.get('/calltaxi/v1/driver/singleview', function(req,res){  
    var id = req.query.id;

    var data = { "Status":"", };
//console.log(req.query);
if(id != '') {
 var query = connection.query('SELECT * from driver WHERE id=?', id, function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Driver View is Cannot Display";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("View Query  "+err);
				res.json({"data":data});
            }else{         
                //res.writeHead(302, {'Location': 'http://localhost/bookmycab/cabadmin/home.html' + req.url});
                //res.end();           			   
                data["Status"] = "Driver Seleted Successfully";
                data["result"] = "Success";     
                //data["records"] = rows; 
                data["name"] = rows[0].name; 
                data["mobile"] = rows[0].mobile; 
                data["email"] = rows[0].email; 
                data["location"] = rows[0].location;
				res.json(data);    

        }  // Update query        
    });   // Insert query
 }else{
        data["Status"] = "Please enter all required data";
        data["result"] = "failure";
				res.json({"data":data});
   } 
});


app.listen(8085);
console.log('port 8085');

