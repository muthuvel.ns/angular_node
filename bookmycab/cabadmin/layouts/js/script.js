var app=angular.module('myApp',['ngRoute']);

app.config(function($routeProvider){

      $routeProvider
          .when('/',{
                templateUrl: 'view/login.html'
          })
// Cabbook
          .when('/home',{
                templateUrl: 'view/home.html'
          })
		  .when('/cabbook',{
			  templateUrl: 'view/booking.html'
		  })
		  .when('/viewbook',{
			  templateUrl: 'view/viewbook.html'
		  })
// Driver 
		  .when('/driver',{
			  templateUrl: 'view/driver.html'
		  })
		  .when('/driverview',{
			  templateUrl: 'view/driver.html'
		  })
		  .when('/createdriver',{
			  templateUrl: 'view/createdriver.html'
		  }); 

});


/*app.controller('MyController',function($scope){
       alert("I am in Script Js");
      $scope.message="Hello world";

}); */
app.config(['$httpProvider', function($httpProvider) {
$httpProvider.defaults.useXDomain = true;
$httpProvider.defaults.headers.common = 'Content-Type: application/json';
delete $httpProvider.defaults.headers.common['X-Requested-With'];
} ]);
// For Login
app.controller('loginController',[ '$scope', '$http','$location', function($scope,$http,$location) {
var _httpPost = $http;
$scope.login = function(uname,pwd) {
$scope.original = {uname:uname, pwd:pwd}; 
_httpPost.post('http://localhost:8085/calltaxi/v1/admin/login', $scope.original)
.then(function(response) {
console.log(response);
if( response.data.Status =='Admin Login successfully'){
$scope.items = response.data;      
$location.path("home");
}
else if( response.data.Status =='Please enter correct username and password'){
$scope.items = response.data;      
$location.path("/");
}
});
};

}]);	// app.controller -- $scope.refresh = function(){

// For Driver
app.controller('MyController', [ '$scope', '$http','$location', function($scope,$http,$location) {
//alert("I am in App Js");
// Full View Driver
$http({
method: 'POST',
url: 'http://localhost:8085/calltaxi/v1/driver/fullviewApi'
}).then(function successCallback(response) {
console.log(response);
var recordsArray = response.data.records;
//console.log(JSON.stringify(recordsArray))
$scope.items = recordsArray;
}, function errorCallback(response) {

});
// update Driver Row
$scope.updateRow = function(id,name,mobile,email,location) {
    $scope.original = {id:id, name:name, mobile:mobile, email:email, location:location}; 
    console.log($scope.original);
    $http.post('http://localhost:8085/calltaxi/v1/driver/updateApi', $scope.original)
    .success(function(response){
        //console.log(response);
        $scope.del = response;
    })
};
// Single View
//var id=$location.search().id;
//console.log($location.search().id);
//$scope.original = {id:id}; 
$scope.sigleRow = function(id) {
$http.get('http://localhost:8085/calltaxi/v1/driver/singleview?id='+id)
    .then(function(response) {
        if(response.data.result =='Success'){
        $scope.user = response.data;
        $location.path("driverview");
        }    
    }) 
};
 
// Delete Driver
$scope.deleteRow = function(id) {
$http.get("http://localhost:8085/calltaxi/v1/driver/deleteApi/?id="+id)
.success(function(response){
//console.log(response.result);
    if(response.result =='Success'){
        $scope.del = response;      
        $location.path("driver");
    }
})
};
// Create Driver
$scope.save = function(name,mobile,email,location) {
$scope.original = {uname:name, location:location, mobile:mobile, email:email}; 
$http.post('http://localhost:8085/calltaxi/v1/driver/createApi', $scope.original)
.then(function(response) {
    console.log(response.data);
    if(response.data.result =='Success'){
        $scope.del = response.data;      
        $location.path("driver");
    }
    });   
}; 

}]);   // app.controller

// For Booking
/* app.controller('bookController',[ '$scope', '$http','$location', function($scope,$http,$location) {
//var _httpPost = $http;
// Full View Driver
$http({
method: 'POST',
url: 'http://localhost:8085/calltaxi/v1/booking/fullviewApi'
}).then(function successCallback(response) {
    console.log(response);
    var recordsArray = response.data.records;
    //console.log(JSON.stringify(recordsArray))
    $scope.items = recordsArray;
}, function errorCallback(response) {

});
// Booking Delete 
$scope.refresh = function(){
$scope.deleteRow = function(id) {
$http.get("http://localhost:8085/calltaxi/v1/booking/deleteApi/?id="+id)
.success(function(response){
    //console.log(response.result);
    if(response.result =='Success'){
        $scope.del = response;      
        $location.path("booking");
    }
})
}; }
// Single View booking
$http({
method: 'GET',
url: 'http://localhost:8085/calltaxi/v1/booking/singleview/?id="+id'
}).then(function successCallback(response) {
    //alert(response.records);
    var recordsArray = response.data.records;
    console.log(JSON.stringify(recordsArray))
    $scope.items = recordsArray;
}, function errorCallback(response) {

});

}]); */	// app.controller -- $scope.refresh = function(){

                                                                                                                                                                                                                                                                 	


