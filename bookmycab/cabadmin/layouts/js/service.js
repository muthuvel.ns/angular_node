var app=angular.module('myApp',['ngRoute']);

app.config(function($routeProvider){
      $routeProvider
          .when('/',{
                templateUrl: 'view/login.html'
          })
// Cabbook
          .when('/home',{
                templateUrl: 'view/home.html'
          })
		  .when('/cabbook',{
			  templateUrl: 'view/booking.html'
		  })
		  .when('/viewbook',{
			  templateUrl: 'view/viewbook.html'
		  })
// Driver 
		  .when('/driver',{
			  templateUrl: 'view/driver.html'
		  })
		  .when('/driverview',{
			  templateUrl: 'view/updatedriver.html'
		  })
		  .when('/createdriver',{
			  templateUrl: 'view/createdriver.html'
		  })
// Car 
		  .when('/cars',{
			  templateUrl: 'view/car.html'
		  })
		  .when('/carview',{
			  templateUrl: 'view/updatecar.html'
		  })
		  .when('/createcar',{
			  templateUrl: 'view/createcar.html'
		  })
// Ticket 
		  .when('/ticket',{
			  templateUrl: 'view/ticket.html'
		  })
		  .when('/ticketview',{
			  templateUrl: 'view/viewticket.html'
		  })
// Payment 
		  .when('/payment',{
			  templateUrl: 'view/payment.html'
		  })
		  .when('/paymentview',{
			  templateUrl: 'view/updatepayment.html'
		  })
		  .when('/createpayment',{
			  templateUrl: 'view/createpayment.html'
		  });
});


/*app.controller('MyController',function($scope){
       alert("I am in Script Js");
      $scope.message="Hello world";

}); */
app.config(['$httpProvider', function($httpProvider) {
$httpProvider.defaults.useXDomain = true;
$httpProvider.defaults.headers.common = 'Content-Type: application/json';
delete $httpProvider.defaults.headers.common['X-Requested-With'];
} ]);
// For Login
app.controller('loginController',[ '$scope', '$http','$location', function($scope,$http,$location) {
var _httpPost = $http;
$scope.login = function(uname,pwd) {
$scope.original = {uname:uname, pwd:pwd}; 
_httpPost.post('http://localhost:8085/calltaxi/v1/admin/login', $scope.original)
.then(function(response) {
console.log(response);
if( response.data.Status =='Admin Login successfully'){
$scope.items = response.data;      
$location.path("home");
}
else if( response.data.Status =='Please enter correct username and password'){
$scope.items = response.data;      
$location.path("/");
}
});
};

}]);	// app.controller -- 

// For Driver
app.controller('driverController', [ '$scope', '$http','$location', function($scope,$http,$location) {
app.config( [ '$locationProvider', function( $locationProvider ) {
   // In order to get the query string from the
   // $location object, it must be in HTML5 mode.
   $locationProvider.html5Mode(true);
}]);

//alert("I am in App Js");
// Full View Driver
$http({
method: 'POST',
url: 'http://localhost:8085/calltaxi/v1/driver/fullviewApi'
}).then(function successCallback(response) {
var recordsArray = response.data.records;
//console.log(JSON.stringify(recordsArray))
$scope.items = recordsArray;
}, function errorCallback(response) {

});
// update Driver Row
$scope.updateRow = function(id,name,mobile,email,location) {
    $scope.original = {id:id, name:name, mobile:mobile, email:email, location:location}; 
    console.log($scope.original);
    $http.post('http://localhost:8085/calltaxi/v1/driver/updateApi', $scope.original)
    .success(function(response){
        //console.log(response);
        $scope.del = response;
        $location.path("driver");
    })
};
// Single View
//var id=$location.search().id;
//console.log($location.search().id);
//$scope.original = {id:id}; 
$scope.singleRow = function(id) {
$http.get('http://localhost:8085/calltaxi/v1/driver/singleview?id='+id)
    .then(function(response) {
        if(response.data.result =='Success'){
        $scope.user = response.data;
        console.log(response.data);
        $location.path("driverview");
        }
    }) 
};
 
// Delete Driver
//$scope.refresh = function(){
$scope.deleteRow = function(id) {
$http.get("http://localhost:8085/calltaxi/v1/driver/deleteApi/?id="+id)
.success(function(response){
//console.log(response.result);
    if(response.result =='Success'){
        $scope.del = response;  
                alert('Successfully Deleted');
        $location.path("driver");
    }
}) // }
};
// Create Driver
$scope.save = function(name,mobile,email,location) {
$scope.original = {uname:name, location:location, mobile:mobile, email:email}; 
$http.post('http://localhost:8085/calltaxi/v1/driver/createApi', $scope.original)
.then(function(response) {
    console.log(response.data);
    if(response.data.result =='Success'){
        $scope.del = response.data; 
                alert('Successfully Created');     
        $location.path("driver");
    }
  if(response.data.result =='failure'){
        $scope.create = response.data;      
        $location.path("createdriver");
    }
    });   
}; 

}]);   // app.controller

// For Booking
 app.controller('bookController',[ '$scope', '$http','$location', function($scope,$http,$location) { 
app.config( [ '$locationProvider', function( $locationProvider ) {
   // In order to get the query string from the
   // $location object, it must be in HTML5 mode.
   $locationProvider.html5Mode(true);
}]);
//var _httpPost = $http;
// Full View Driver
$http({
method: 'GET',
url: 'http://localhost:8085/calltaxi/v1/booking/fullviewApi'
}).then(function successCallback(response) {
    //console.log(response);
    var recordsArray = response.data.records;
    console.log(JSON.stringify(recordsArray))
    $scope.items = recordsArray;
}, function errorCallback(response) {

});
// Booking Delete 
//$scope.refresh = function(){
$scope.deleteRow = function(id) {
$http.get("http://localhost:8085/calltaxi/v1/booking/deleteApi/?id="+id)
.success(function(response){
    //console.log(response.result);
    if(response.result =='Success'){
        $scope.del = response;
                alert('Successfully Deleted');
        $location.path("cabbook");
    }
})
// } 
};

// Single View booking
$scope.singleRow = function(id) {

$http.get('http://localhost:8085/calltaxi/v1/booking/singleview?id='+id)
    .then(function(response) {
//console.log(response.data.result);        
console.log(response);
        if(response.data.result =='Success'){
console.log(JSON.stringify(response.data.records[0]));
        $scope.user = JSON.stringify(response.data.records[0]);
        $location.path("viewbook");
        }
    })
};


}]); 	// app.controller  bookviewController

// For Car
 app.controller('carController',[ '$scope', '$http','$location', function($scope,$http,$location) { 
app.config( [ '$locationProvider', function( $locationProvider) {
   // In order to get the query string from the
   // $location object, it must be in HTML5 mode.
   $locationProvider.html5Mode(true);
}]);
// Full View car
    $http({
  method: 'GET',
  url: 'http://localhost:8085/calltaxi/v1/car/fullviewApi'
}).then(function successCallback(response) {
//alert(response.records);
                var recordsArray = response.data.records;
            console.log(JSON.stringify(recordsArray))
            $scope.items = recordsArray;
  }, function errorCallback(response) {

  });
   // Create car
	$scope.save = function(uname,cid,model,color,number) {
	$scope.original = {name:uname, cid:cid, model:model, color:color, number:number}; 
    $http.post('http://localhost:8085/calltaxi/v1/car/createApi', $scope.original)
	.then(function(response) {
                //console.log(response.data);
                if(response.data.result =='Success'){
                    $scope.del = response.data;    
                alert('Successfully Created');  
                $location.path("cars");
                 }
    });   
  }; 
// update car Row
        $scope.updateRow = function(id) {
                $http.get("http://localhost:8085/calltaxi/v1/car/singleview/?id="+id)
                .success(function(response){
                //console.log(response.data);
                if(response.data.result =='Success'){
                    $scope.del = response.data;      
                $location.path("cars");
                 }
                })
               };
   // Delete car
        $scope.deleteRow = function(id) {
                $http.get("http://localhost:8085/calltaxi/v1/car/deleteApi/?id="+id)
                .success(function(response){
                if(response.result =='Success'){
                    $scope.del = response;   
                alert('Successfully Deleted');   
                $location.path("cars");
                 }
                })
               };
// Single View car
        $scope.singleRecord = function(id) {
                $http.get("http://localhost:8085/calltaxi/v1/car/singleview/?id="+id)
                .success(function(response){
                $scope.items = response;
                $location.path("carview");
                })
               };
}]); // car controller 

// For Payment
 app.controller('payController',[ '$scope', '$http','$location', function($scope,$http,$location) { 
app.config( [ '$locationProvider', function( $locationProvider) {
   $locationProvider.html5Mode(true);
}]);
         
         
// Full View payment
    $http({
  method: 'POST',
  url: 'http://localhost:8085/calltaxi/v1/payment/fullviewApi'
}).then(function successCallback(response) {
//alert(response.records);
                var recordsArray = response.data.records;
            console.log(JSON.stringify(recordsArray))
            $scope.items = recordsArray;
  }, function errorCallback(response) {

  });  
// Create payment
	$scope.save = function(km,amount) {
	$scope.original = {km:km, amount:amount}; 
// alert(km);
    $http.post('http://localhost:8085/calltaxi/v1/payment/createApi', $scope.original)  
	.then(function(response) {
                if(response.data.result =='failure'){
                    $scope.del = response.data;      
                 }
                else if(response.data.result =='Success'){
                    $scope.del = response.data;
                alert('Successfully Created');      
                $location.path("payment");
                 }
    });   

  };      

   // Delete payment
        $scope.deleteRow = function(id) {
                $http.get("http://localhost:8085/calltaxi/v1/payment/deleteApi/?id="+id)
                .success(function(response){
                    console.log(response);
                 if(response.result =='Success'){
                    $scope.del = response;                        
                //$location.path("home");
                alert('Successfully Deleted');
                $location.path("payment");
                //windows.reload();
                  }
                else if(response.data.result =='failure'){
                    $scope.del = response;      
                $location.path("payment");
                 }
                });

               };
                                                                                                                                                                                                                                         
}]); //payController


// For Ticket
 app.controller('ticketController',[ '$scope', '$http','$location', function($scope,$http,$location) { 
app.config( [ '$locationProvider', function( $locationProvider) {
   $locationProvider.html5Mode(true);
}]);

// Ticketing Full View
    $http({
  method: 'GET',
  url: 'http://localhost:8085/calltaxi/v1/ticket/fullviewApi'
}).then(function successCallback(response) {
                var recordsArray = response.data.records;
            console.log(JSON.stringify(recordsArray))
            $scope.items = recordsArray;
  }, function errorCallback(response) {

  });
  // Ticketing Delete 
        $scope.deleteRow = function(id) {
                $http.get("http://localhost:8085/calltaxi/v1/ticket/deleteApi/?id="+id)
                .success(function(response){
                    console.log(response);
                 if(response.result =='Success'){
                    $scope.del = response;                        
                //$location.path("home");
                alert('Successfully Deleted');
                $location.path("payment");
                windows.reload();
                  }
                else if(response.data.result =='failure'){
                    $scope.del = response;      
                $location.path("payment");
                 }
                });

               };
                
// Single View car
        $scope.singleRow = function(id) {
                $http.get("http://localhost:8085/calltaxi/v1/ticket/singleview/?id="+id)
                .success(function(response){
                $scope.items = response;
                $location.path("ticketview");
                })
               };

}]); // ticketController

