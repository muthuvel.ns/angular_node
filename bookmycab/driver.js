/* 
// Admin Login
URL : http://localhost:8085/calltaxi/v1/admin/login
Admin Login Api  --- admin/login
// Ticket
URL : http://localhost:8085/calltaxi/v1/ticket/
1 Ticket Delete Api  --- deleteApi
2 Ticket Full View Api --- fullviewApi
3 Ticket Records Single View Api --- singleview
// Booking
URL : http://localhost:8085/calltaxi/v1/booking/
1 Booking Delete Api  --- deleteApi
2 Booking Full View Api --- fullviewApi
3 Booking Records Single View Api --- singleview
// Driver
URL : http://localhost:8085/calltaxi/v1/driver/
1 Create New Driver Api --- createApi
2 Driver Full View Api --- fullviewApi
3 Driver update Api --- updateApi
4 Driver Records Delete Api --- deleteApi
5 Driver Records Single View Api --- singleview

URL : http://localhost:8085/calltaxi/v1/car/
1 Create New Car Api --- createApi
2 Car Full View Api --- fullviewApi
3 Car update Api --- updateApi
4 Car Records Delete Api --- deleteApi
5 Car Records Single View Api --- singleview

// Payment
URL : http://localhost:8085/calltaxi/v1/payment/
1 Create New Payment Api --- createApi
2 Payment Full View Api --- fullviewApi
3 Payment update Api --- updateApi
4 Payment Records Delete Api --- deleteApi
5 Payment Records Single View Api --- singleview
*/

var app = require('express')(); // Express App include
var http = require('http').Server(app); // http server
var mysql = require('mysql'); // Mysql include
//var mailer = require("nodemailer");
var bodyParser = require("body-parser"); // Body parser for fetch posted data
var connection = mysql.createConnection({ // Mysql Connection
    host : 'localhost',
    user : 'root',
    password : 'root',
    database : 'cabdb',
});
var crypto = require('crypto');
var request = require('request');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(function(req, res, next) {
res.setHeader('Access-Control-Allow-Origin', '*');
res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
next();
});


// 1 Booking Full View Api
app.get('/calltaxi/v1/booking/fullviewApi', function(req,res){   
    var data = { "Status":"", };
//console.log(req.body);
 var query = connection.query('SELECT * from book ORDER BY id DESC', function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Cannot Search booking View";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("SELECT Query  "+err);
				res.json(data);
            }else{                   
				
				data['records']=rows;
                data["Status"] = "Booking select Successfully";
                data["result"] = "Success";             
				res.json(data);			    
            // res.json(data);
        }  // Update query
        
    });   // Insert query
});


// 2 Booking Records Delete Api
app.get('/calltaxi/v1/booking/deleteApi', function(req,res){  
    var id = req.query.id;
    var data = { "Status":"", };
console.log(req.query);
if((id != '') && (id >= 1)) {
 var query = connection.query('DELETE from book WHERE id=?', id, function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Booking Cannot Delete";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("Delete Query  "+err);
				res.json(data);
            }else{                  		   
                data["Status"] = "Booking Deleted Successfully";
                data["result"] = "Success";             
				res.json(data);
        }  // Update query        
    });   // Insert query
 }else{
        data["Status"] = "Please enter all required data";
        data["result"] = "failure";
				res.json(data);
   } 
});

// 3 Booking Records Single View Api
app.get('/calltaxi/v1/booking/singleview', function(req,res){  
    var id = req.query.id;

    var data = { "Status":"", };
//console.log(req.query);
if(id != '') {
 var query = connection.query('SELECT * from book WHERE id=?', id, function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Booking View is Cannot Display";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("View Query  "+err);
				res.json(data);
            }else{         
                //res.writeHead(302, {'Location': 'http://localhost/bookmycab/cabadmin/home.html' + req.url});
                //res.end();           			   
                data["Status"] = "Booking Seleted Successfully";
                data["result"] = "Success";     
                data["records"] = rows;                 
				res.json(data);    

        }  // Update query        
    });   // Insert query
 }else{
        data["Status"] = "Please enter all required data";
        data["result"] = "failure";
				res.json(data);
   } 
});

// 1 Ticket Full View Api
app.get('/calltaxi/v1/ticket/fullviewApi', function(req,res){   
    var data = { "Status":"", };
//console.log(req.body);
 var query = connection.query('SELECT * from ticket ORDER BY id DESC', function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Cannot Search Ticketing View";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("SELECT Query  "+err);
				res.json(data);
            }else{                   
				
				data['records']=rows;
                data["Status"] = "Ticketing select Successfully";
                data["result"] = "Success";             
				res.json(data);			    
            // res.json(data);
        }  // Update query
        
    });   // Insert query
});


// 2 Ticketing Records Delete Api
app.get('/calltaxi/v1/ticket/deleteApi', function(req,res){  
    var id = req.query.id;
    var data = { "Status":"", };
console.log(req.query);
if((id != '') && (id >= 1)) {
 var query = connection.query('DELETE from ticket WHERE id=?', id, function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Ticketing Cannot Delete";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("Delete Query  "+err);
				res.json(data);
            }else{                  		   
                data["Status"] = "Ticketing Deleted Successfully";
                data["result"] = "Success";             
				res.json(data);
        }  // Update query        
    });   // Insert query
 }else{
        data["Status"] = "Please enter all required data";
        data["result"] = "failure";
				res.json(data);
   } 
});

// 3 Ticketing Records Single View Api
app.get('/calltaxi/v1/ticket/singleview', function(req,res){  
    var id = req.query.id;

    var data = { "Status":"", };
//console.log(req.query);
if(id != '') {
 var query = connection.query('SELECT * from ticket WHERE id=?', id, function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Ticketing View is Cannot Display";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("View Query  "+err);
				res.json(data);
            }else{         
                //res.writeHead(302, {'Location': 'http://localhost/bookmycab/cabadmin/home.html' + req.url});
                //res.end();           			   
                data["Status"] = "Ticketing Seleted Successfully";
                data["result"] = "Success";     
                data["records"] = rows;                 
				res.json(data);    

        }  // Update query        
    });   // Insert query
 }else{
        data["Status"] = "Please enter all required data";
        data["result"] = "failure";
				res.json(data);
   } 
});

// 1. Create New Driver Api
app.post('/calltaxi/v1/driver/createApi', function(req,res){    
    var name = req.body.uname;
    var mobile = req.body.mobile;
    var location = req.body.location;
    var email = req.body.email;  
var pos = {
        name:name,
        mobile: mobile,
        email: email,
        location: location
    };
    var data = { "Status":"", };
// console.log(req.body);
if((name != '') && (mobile != '')) {

 var query = connection.query('INSERT INTO driver SET ?', pos, function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Driver Cannot Create";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("Insert Query  "+err);
				res.json(data);
            }else{  
                 connection.query('update driver set did=CONCAT("CD", LAST_INSERT_ID()) where id=LAST_INSERT_ID()', 
                 function(err, rows, fields){
                 if(!!err){
				data["result"] = "failure";
                console.log("Update Query Error  "+err);
				res.json(data);
			    }else{					
                data["Status"] = "Driver Created Successfully";				
                data["result"] = "Success";             
				res.json(data);
			    } //res.json(data);
            }); // Update query            
        }         
    });   
 }else{
        data["Status"] = "Please enter all required data";
        data["result"] = "failure";
        res.json(data);
		} 
	});


// 2 Driver Full View Api
app.post('/calltaxi/v1/driver/fullviewApi', function(req,res){   
    var data = { "Status":"", };
//console.log(req.query);
 var query = connection.query('SELECT * from driver order by id desc', function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Cannot Search Driver View";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("SELECT Query  "+err);
				res.json(data);
            }else{                   
				
				data['records']=rows;
                data["Status"] = "Driver select Successfully";
                data["result"] = "Success";             
				res.json(data);			    
            // res.json(data);
        }  // Update query
        
    });   // Insert query
});



// 3 Driver update Api updateApi
app.post('/calltaxi/v1/driver/updateApi', function(req,res){ 
    var id = req.body.id;
	var name = req.body.name;
    var mobile = req.body.mobile;
    var location = req.body.location;
    var email = req.body.email; 
var pos = {
        id:id,
        name:name,
        mobile: mobile,
        email: email,
        location: location
    };
    var data = { "Status":"", };
console.log(req.body);
if(id != '')  {
 var query = connection.query('UPDATE driver SET ? WHERE id=?', [pos,id], function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Driver Cannot Update";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("UPDATE Query  "+err);
            }else{                   			   
                data["Status"] = "Driver Updated Successfully";
                data["result"] = "Success"; 
                //console.log(query.sql)            
				res.json(data);
        }  // Update query        
    });   // Insert query
 }else{
        data["Status"] = "Please enter all required data";
        data["result"] = "failure";
				res.json(data);
		} 
	});




// 4 Driver Records Delete Api
app.get('/calltaxi/v1/driver/deleteApi', function(req,res){  
    var id = req.query.id;
    var data = { "Status":"", };
console.log(req.query);
if((id != '') && (id >= 1)) {
 var query = connection.query('DELETE from driver WHERE id=?', id, function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Driver Cannot Delete";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("Delete Query  "+err);
				res.json(data);
            }else{                  		   
                data["Status"] = "Driver Deleted Successfully";
                data["result"] = "Success";             
				res.json(data);
        }  // Update query        
    });   // Insert query
 }else{
        data["Status"] = "Please enter all required data";
        data["result"] = "failure";
				res.json(data);
   } 
});



// 5 Driver Records Single View Api
app.get('/calltaxi/v1/driver/singleview', function(req,res){  
    var id = req.query.id;

    var data = { "Status":"", };
console.log(req.query);
if(id != '') {
 var query = connection.query('SELECT * from driver WHERE id=?', id, function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Driver View is Cannot Display";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("View Query  "+err);
				res.json(data);
            }else{         
                //res.writeHead(302, {'Location': 'http://localhost/bookmycab/cabadmin/home.html' + req.url});
                //res.end();           			   
                data["Status"] = "Driver Seleted Successfully";
                data["result"] = "Success";     
                //data["records"] = rows; 
                data["id"] = rows[0].id;
                data["name"] = rows[0].name; 
                data["mobile"] = rows[0].mobile; 
                data["email"] = rows[0].email; 
                data["location"] = rows[0].location;
				res.json(data);    

        }  // Update query        
    });   // Insert query
 }else{
        data["Status"] = "Please enter all required data";
        data["result"] = "failure";
				res.json(data);
   } 
});

// 1 Create New Car Api
app.post('/calltaxi/v1/car/createApi', function(req,res){    
    var name = req.body.name;
    var cid = req.body.cid;
    var model = req.body.model;
    var color = req.body.color;
    var number = req.body.number;
var pos = {
        name:name,
        cid:cid,
        model: model,
        color: color,
        number: number
    };
    var data = { "Status":"", };
 //console.log(req.body);
if((name != '') && (model != '')&& (number != '')&& (color != '')) {
 var query = connection.query('INSERT INTO car SET ?', pos, function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Car Cannot Create";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("Insert Query  "+err);
				res.json(data);
            }else{  
                 connection.query('update car SET cid=CONCAT("CD", LAST_INSERT_ID()) where id=LAST_INSERT_ID()', 
                 function(err, rows, fields){
                 if(!!err){
				data["result"] = "failure";
                console.log("Update Query Error"+err);
				res.json(data);
			    }else{					
                data["Status"] = "Car Created Successfully";				
                data["result"] = "Success";             
				res.json(data);
			    } //res.json(data);
            }); // Update query            
        }         
    });   
 }else{
        data["Status"] = "Please enter all required data";
        data["result"] = "Success";
        res.json(data);
		} 
	});


// 2 Car Full View Api
app.get('/calltaxi/v1/car/fullviewApi', function(req,res){   
    var data = { "Status":"", };
//console.log(req.body);
 var query = connection.query('SELECT * from car ORDER BY id DESC', function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Cannot Search Car View";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("SELECT Query  "+err);
				res.json(data);
            }else{ 
				data['records']=rows;
                data["Status"] = "car select Successfully";
                data["result"] = "Success";             
				res.json(data);			    
        }  // Update query
        
    });   // Insert query
});



// 3 Car update Api updateApi
app.post('/calltaxi/v1/car/updateApi', function(req,res){ 
   var id = req.body.id;
   var cid = req.body.cid;
   var name = req.body.name;
    var model = req.body.model;
    var color = req.body.color;
    var number = req.body.number;  
var pos = {
        id:id,
        cid: cid,
        name:name,
        model: model,
        color: color,
        number: number
    };
    var data = { "Status":"", };
// console.log(req.body);
if((name != '') && (model != '') && (number != '') && (id != '')) {
 var query = connection.query('UPDATE car SET ? WHERE id=?', [pos,id], function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Car Cannot Update";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("UPDATE Query  "+err);
            }else{                   			   
                data["Status"] = "Car Updated Successfully";
                data["result"] = "Success"; 
                //console.log(query.sql)            
				res.json(data);
        }  // Update query        
    });   // Insert query
 }else{
        data["Status"] = "Please enter all required data";
        data["result"] = "Success";
				res.json(data);
		} 
	});


// 4 Car Records Delete Api
app.get('/calltaxi/v1/car/deleteApi', function(req,res){  
    var id = req.query.id;
    var api = req.query.api;
    var data = { "Status":"", };
//console.log(req.query);
if((id != '') && (id >= 1)) {
 var query = connection.query('DELETE from car WHERE id=?', id, function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Car Cannot Delete";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("Delete Query  "+err);
				res.json(data);
            }else{                  		   
                data["Status"] = "Car Deleted Successfully";
                data["result"] = "Success";             
				res.json(data);
        }  // Update query        
    });   // Insert query
 }else{
        data["Status"] = "Please enter all required data";
        data["result"] = "Success";
				res.json(data);
   } 
});


// 5 Car Records Single View Api
app.get('/calltaxi/v1/car/singleview', function(req,res){  
    var id = req.query.id;

    var data = { "Status":"", };
//console.log(req.query);
if(id != '') {
 var query = connection.query('SELECT * from car WHERE id=?', id, function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Car View is Cannot Display";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("View Query  "+err);
				res.json(data);
            }else{         
                //res.writeHead(302, {'Location': 'http://localhost/bookmycab/cabadmin/home.html' + req.url});
                //res.end();           			   
                data["Status"] = "Car Seleted Successfully";
                data["result"] = "Success";     
                data["records"] = rows; 
				res.json(data);    

        }  // Update query        
    });   // Insert query
 }else{
        data["Status"] = "Please enter all required data";
        data["result"] = "Success";
				res.json(data);
   } 
});

//  Admin Login Api  
app.post('/calltaxi/v1/admin/login', function(req,res,next){  

    var uname = req.body.uname;
    var pwd = req.body.pwd;

    var data = { "Status":"", };
//console.log(req.body);
if((uname != '') && (pwd != '')) {
 
var cryptpwd= crypto.createHash('md5').update(pwd).digest("hex");

 var query = connection.query('SELECT * from loginadmin WHERE username=? and password=?', [uname,cryptpwd], function(err, rows, fields){
            if(err){                               			   
               data["Status"] = "Admin Login is Error";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("View Query  "+err);
				res.json(data);
              }
            else if(rows.length != 0){
                 data["Status"] = "Admin Login successfully";
                data["result"] = "Success";     
                data["id"] = rows[0].id; 
                data["uname"] = rows[0].username;
				res.json(data);  
            }else{   
                data["Status"] = "Please enter correct username and password";
                data["result"] = "Success";
                //console.log(query.sql);
				res.json(data);       

        }  // else  

    });   // Insert query
				//res.json({"data":data}); 
 }else{
        data["Status"] = "Please enter all required data";
        data["result"] = "success";
        res.json(data);
   } 
});

// 1 Create New Payment Api
app.post('/calltaxi/v1/payment/createApi', function(req,res){    
    var km = req.body.km;
    var amount = req.body.amount;
var pos = {
        km: km,
        amount: amount
    };
    var data = { "Status":"", };
 console.log(req.body);
if((km != '') && (amount != '')) {
 var query = connection.query('INSERT INTO payment SET ?', pos, function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Payment Cannot Create";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("Insert Query  "+err);
				res.json(data);
            }else{  
                data["Status"] = "Payment Created Successfully";				
                data["result"] = "Success";             
				res.json(data);           
        }         
    });   
 }else{
        data["Status"] = "Please enter all required data";
        data["result"] = "Success";
        res.json(data);
		} 
	});


// 2 Payment Full View Api
app.post('/calltaxi/v1/payment/fullviewApi', function(req,res){   
    var data = { "Status":"", };
//console.log(req.body);
 var query = connection.query('SELECT * from payment ORDER BY id DESC', function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Cannot Search Payment View";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("SELECT Query  "+err);
				res.json(data);
            }else{ 
				data['records']=rows;
                data["Status"] = "Payment select Successfully";
                data["result"] = "Success";             
				res.json(data);			    
        }  // Update query
        
    });   // Insert query
});


// 3 Payment update Api updateApi
app.post('/calltaxi/v1/payment/updateApi', function(req,res){ 
   var km = req.body.km;
    var amount = req.body.amount;  
var pos = {
        km:km,
        amount: amount
    };
    var data = { "Status":"", };
// console.log(req.body);
if((km != '') && (amount != '')) {
 var query = connection.query('UPDATE payment SET ? WHERE id=?', [pos,id], function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Payment Cannot Update";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("UPDATE Query  "+err);
            }else{                   			   
                data["Status"] = "Payment Updated Successfully";
                data["result"] = "Success"; 
                //console.log(query.sql)            
				res.json(data);
        }  // Update query        
    });   // Insert query
 }else{
        data["Status"] = "Please enter all required data";
        data["result"] = "Success";
				res.json(data);
		} 
	});


// 4 Payment Records Delete Api
app.get('/calltaxi/v1/payment/deleteApi', function(req,res){  
    var id = req.query.id;
    var api = req.query.api;
    var data = { "Status":"", };
//console.log(req.query);
if((id != '') && (id >= 1)) {
 var query = connection.query('DELETE from payment WHERE id=?', id, function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Record Cannot Delete";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("Delete Query  "+err);
				res.json(data);
            }else{                  		   
                data["Status"] = "Payment Deleted Successfully";
                data["result"] = "Success";             
				res.json(data);
        }  // Update query        
    });   // Insert query
 }else{
        data["Status"] = "Please enter all required data";
        data["result"] = "Success";
				res.json(data);
   } 
});


// 5 Payment Records Single View Api
app.get('/calltaxi/v1/payment/singleview', function(req,res){  
    var id = req.query.id;

    var data = { "Status":"", };
//console.log(req.query);
if(id != '') {
 var query = connection.query('SELECT * from payment WHERE id=?', id, function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Payment View is Cannot Display";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("View Query  "+err);
				res.json(data);
            }else{         
                //res.writeHead(302, {'Location': 'http://localhost/bookmycab/cabadmin/home.html' + req.url});
                //res.end();           			   
                data["Status"] = "Payment Seleted Successfully";
                data["result"] = "Success";     
                //data["records"] = rows; 
                data["km"] = rows[0].km; 
                data["amount"] = rows[0].amount; 
				res.json(data);
        }  // Update query        
    });   // Insert query
 }else{
        data["Status"] = "Please enter all required data";
        data["result"] = "Success";
				res.json(data);
   } 
});


// 2 Settings View Api
app.get('/calltaxi/v1/admin/settingsApi', function(req,res){   
    var data = { "Status":"", };
//console.log(req.body);
 var query = connection.query('SELECT * from settings', function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Cannot Search settings View";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("SELECT Query  "+err);
				res.json(data);
            }else{ 
				data['records']= rows;
                data["Status"] = "Settings select Successfully";
                data["result"] = "Success";             
				res.json(data);			    
        }  // Update query
        
    });   // Insert query
});



// 1 Car update Api updateApi
app.post('/calltaxi/v1/car/updateApi', function(req,res){ 
   var name = req.body.name;
    var model = req.body.model;
    var color = req.body.color;
    var number = req.body.number;  
var pos = {
        name:name,
        model: model,
        color: color,
        number: number
    };
    var data = { "Status":"", };
// console.log(req.body);
if((name != '') && (model != '')&& (number != '') && (id != '')) {
 var query = connection.query('UPDATE car SET ? WHERE id=?', [pos,id], function(err, rows, fields){
// if(rows.length != 0){
            if(err){
                data["Status"] = "Car Cannot Update";
                data["result"] = "failure";
                data["error"] = err;
                console.log(query.sql);
                console.log("UPDATE Query  "+err);
            }else{                   			   
                data["Status"] = "Car Updated Successfully";
                data["result"] = "Success"; 
                //console.log(query.sql)            
				res.json(data);
        }  // Update query        
    });   // Insert query
 }else{
        data["Status"] = "Please enter all required data";
        data["result"] = "Success";
				res.json(data);
		} 
	});

app.listen(8085);
console.log('port 8085');

