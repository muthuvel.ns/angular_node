// app.js
'use strict';
var app=angular.module('myApp', ['ngRoute']);
app.config(['$routeProvider', function($routeProvider){
$routeProvider.when('/login', {templateUrl:'login.html', controller:'loginCtrl'});
$routeProvider.when('/home', {templateUrl: 'home.html', controller: 'homectrl'});
$routeProvider.otherwise({redirectTo: '/login'});
}]);

app.run(function($rootScope, $location, loginService){
var routespermission=['/home'];
$rootScope.$on('$routeChangeStart', function() {
console.log('>>exit tab: '+routespermission.indexof($location.path()));
console.log('>>logged: '+ loginService.islogged());
if(routepermission.indexof($location.path()) !=-1 && !loginService.islogged())
{
    $location.path('/login');
}
});
});
